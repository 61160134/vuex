const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/:message', function (req, res, next) {
  const { params } = req
  res.json({ message: 'Hello Nutchaya!!' , params })
  // res.send('Hello my name is Nutchaya !')
})



module.exports = router